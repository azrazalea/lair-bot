* Lair Bot
A discord bot for my personal use

** Requirements
Theoretically Lair Bot will run on any modern common lisp implementation on any platform that all the dependencies
are implemented. In practice, it has only been tested on Debian's SBCL 2.1.0 package running on Debian Testing linux.

** Setup
1. Setup a discord bot using [[https://discordpy.readthedocs.io/en/latest/discord.html][discord's instructions]] and invite it to your server.
2. Install [[http://sbcl.org/][SBCL]] using your package manager, binary, or from source.
3. Install quicklisp using the instructions [[https://www.quicklisp.org/beta/][here]].
4. Open a SBCL REPL that has quicklisp loaded and load the direct dependencies via ~(ql:quickload "<dependency>")~. This will download the dependencies to your system.
   - Those dependencies are currently as of this writing ~:lispcord :str :local-time :defstar :iterate :closer-mop~
5. Put this git repo somewhere ASDF can find it. An easy place on linux with quicklisp setup is =~/quicklisp/local-projects/=.
6. In a SBCL REPL load Lair Bot using ~(asdf:load-system :lair-bot)~.
7. Switch to the lair-bot package ~(in-package :lair-bot)~.
8. Create a new configuration object. ~(setf *config* (make-bot-config :token "<your discord bot token>" :prefix "<your bot prefix, default !m>"))~.
9. Create a new bot object with that configuration. ~(setf *my-bot* (make-instance 'lair-bot :config *config*))~
10. Save your config to the config file. ~(save-config *my-bot*)~. This will write a ~config.lisp~ to the root directory of the Lair Bot project. Don't worry, it is ignored by git!
11. Use the easy start. ~(start-lair-bot)~. This will create a new bot and set it to ~*lair-bot*~, loading the config from the file you just created.
    - In the future you can start at this step. Use ~(stop-lair-bot)~ to disconnect the bot properly.
12. Play with the bot on your server! Send it the command ~help~ to see what is currently available.

** Dependencies
These are all available via quicklisp (see Setup).
- [[https://github.com/pcostanza/closer-mop][closer-mop]]
- [[https://github.com/lisp-mirror/defstar][defstar]]
- [[https://common-lisp.net/project/iterate/][iterate]]
- [[https://github.com/lispcord/lispcord][lispcord]]
- [[https://common-lisp.net/project/local-time/][local-time]]
- [[https://github.com/vindarel/cl-str][str]]

Flattened indirect dependencies as of last update of this document.
- [[https://common-lisp.net/project/alexandria/][alexandria]]
- [[https://common-lisp.net/project/babel/][babel]]
- [[https://common-lisp.net/project/bordeaux-threads/][bordeaux-threads]]
- [[https://common-lisp.net/project/cffi/][CFFI, CFFI-grovel, CFFI-toolchain]]
- [[https://common-lisp.net/project/clbuild/mirror/chipz/doc/chipz.html][Chipz]]
- [[http://edicl.github.io/chunga/][Chunga]]
- [[https://github.com/m2ym/cl-annot][cl-annot]]
- [[https://www.cliki.net/cl-base64][cl-base64]]
- [[https://github.com/rudolfochrist/cl-change-case][cl-change-case]]
- [[http://edicl.github.io/cl-ppcre/][CL-PPCRE and CL-PPCRE-UNICODE]]
- [[https://github.com/m2ym/cl-syntax][CL-SYNTAX]]
- [[http://edicl.github.io/cl-unicode/][cl-unicode]]
- [[https://common-lisp.net/project/cl-utilities/][cl-utilities]]
- [[https://common-lisp.net/project/cl-plus-ssl/][cl+ssl]]
- [[https://github.com/Shinmera/dissect][Dissect]]
- [[https://github.com/Shinmera/documentation-utils][documentation-utils]]
- [[https://edicl.github.io/drakma/][Drakma]]
- [[https://github.com/fukamachi/event-emitter][event-emitter]]
- [[https://github.com/fukamachi/fast-http][fast-http]]
- [[https://github.com/rpav/fast-io][fast-io]]
- [[https://github.com/fukamachi/fast-websocket][fast-websocket]]
- [[http://edicl.github.io/flexi-streams/][flexi-streams]]
- [[https://github.com/sharplispers/ironclad][ironclad]]
- [[https://github.com/Rudolph-Miller/jonathan][Jonathan]]
- [[https://github.com/melisgl/named-readtables][named-readtables]]
- [[https://github.com/Shinmera/piping][piping]]
- [[https://quickref.common-lisp.net/proc-parse.html][proc-parse]]
- [[https://www.cliki.net/puri][Puri]]
- [[https://github.com/fukamachi/quri][quri]]
- [[https://www.cliki.net/split-sequence][split-sequence]]
- [[https://quickref.common-lisp.net/smart-buffer.html][smart-buffer]]
- [[https://github.com/sionescu/static-vectors][static-vectors]]
- [[https://github.com/trivial-features/trivial-features][trivial-features]]
- [[https://common-lisp.net/project/trivial-garbage/][trivial-garbage]]
- [[https://github.com/trivial-gray-streams/trivial-gray-streams][trivial-gray-streams]]
- [[https://github.com/Shinmera/trivial-indent][trivial-indent]]
- [[https://github.com/m2ym/trivial-types][trivial-types]]
- [[https://gitlab.common-lisp.net/trivial-utf-8/trivial-utf-8][trivial-utf-8]]
- [[https://common-lisp.net/project/asdf/uiop.html][uiop]]
- [[https://common-lisp.net/project/usocket/][usocket]]
- [[https://github.com/Shinmera/verbose][verbose]]
- [[https://github.com/fukamachi/websocket-driver][websocket-driver-base and websocket-driver-client]]
- [[https://github.com/fukamachi/xsubseq][xsubseq]]
