;;;; lair-bot.asd

(asdf:defsystem #:lair-bot
  :description "My personal discord bot"
  :author "Lily Carpenter <lily-license@azrazalea.net>"
  :license "AGPLv3"
  :serial t
  :depends-on ("lispcord"
               "str"
               "local-time"
               "defstar"
               "iterate"
               "closer-mop")
  :pathname "src"
  :components ((:file "package")
               (:file "command")
               (:file "main")
               (:module commands
                :serial t
                :components ((:file "package")
                             (:file "help")
                             (:file "ping")
                             (:file "uptime")
                             (:file "calc")))))
