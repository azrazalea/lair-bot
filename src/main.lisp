;;;; main.lisp

(in-package #:lair-bot)

;;; Configure local-time to use UTC
(setf local-time:*default-timezone* local-time:+utc-zone+)

;;; XXX: Is the lack of :documentation on struct slots annoying enough to mean
;;;      I should just go ahead and make this a class with a printer/reader?
(defstruct bot-config
  "Basic struct to serialize/read admin settable configuration options from file
Anything the admin should be expected to be able to load/save for the bot at
bot spawn time should be here.
<token>: String containing the discord bot token for authentication.
<prefix>: String prefix for bot commands in non-DM channels."
  (token nil :type (or null string))
  (prefix "!m" :type string))

;;; XXX: Depending on how I implement the bigger more modular features I may want this to become a class
;;; of its own to allow for inheritance.
(defstruct bot-state
  "Basic struct to serialize/read current state of the bot.
The reason this is a separate struct instead of just data
on the bot class itself is for the idea of easily being able to
save the state of the bot into a file and load it again instead of losing data.
This may be able to provide live backups/restores and save data upon bot shutdown.
<spawn-timestamp>: When this instance of the bot was first brought up."
  (spawn-timestamp (local-time:now) :type local-time:timestamp)
  ;;; XXX: Do I want this to be kept when loading from a backup/shutdown or
  ;;;      should uptime and related mean the time this current code instance has been up?
  )

(defclass lair-bot ()
  ((config :type (or null bot-config) :initarg :config :initform nil :documentation "Contains configuration data, set by (load-config) and saved to file by (save-config).")
   (config-file-name :type string :initform "config.lisp" :documentation "File name for the current config file.")
   (config-file-path :type (or null pathname) :initform nil :documentation "File path for the current config file. Used to cache (find-config)")
   (state :type bot-state :accessor state :initform (make-bot-state) :documentation "Contains volatile data of current bot instance.")
   (lc-bot :type lispcord.core:bot :reader delegate :documentation "Delegate object for bot functionality, this is the lispcord bot object.")
   ;;;      XXX: Figure out a better name for reader than 'delegate'.
   )
  (:documentation "Main class for the bot. Instead of inheriting from lispcord.core:bot I
decided to use composition and store the lispcord bot instance to delegate things to.
Most functions in all parts of the bot code will likely directly or indirectly interface with this class."))

(defmethod* (initialize-instance -> lair-bot) :after ((bot lair-bot) &key)
  "Initializes a bot instance. This has the side effects of loading the config file
as well as setting up the lispcord bot object but does NOT connect to discord."
  (with-slots (config lc-bot) bot
    (unless config
      (setf config (load-config bot)))
    (setf lc-bot (lispcord:make-bot (bot-config-token config))))
  bot)

(defgeneric find-config (obj &optional reload-config-path))
(defmethod* (find-config -> pathname) ((bot lair-bot) &optional ((reload-config-path boolean) nil))
  "Creates the pathname object for the config file, caches using bot object slot config-file-path
<reload-config-path>: Optional, defaults to nil. Forces the pathname object to be recreated if a trueable."
  (with-slots (config-file-path config-file-name) bot
      (when (or (not config-file-path) reload-config-path)
        (let ((basedir
                (uiop/pathname:pathname-parent-directory-pathname #.(or *compile-file-truename* *load-truename*))))
          (setf config-file-path (merge-pathnames basedir config-file-name))))
    config-file-path))

(defgeneric load-config (obj &optional reload-config-path))
(defmethod* (load-config -> bot-config) ((bot lair-bot) &optional ((reload-config-path boolean) nil))
  "Loads the config object from the config file and saves it to <bot>
<bot>: The bot object to save the config to.
<reload-config-path>: Optional, defaults to nil.
                      Passed to config-file-path to force pathname object to be recreated if a trueable."
  (with-open-file (in (find-config bot reload-config-path)
                      :external-format :utf-8)
    (setf (slot-value bot 'config) (read in))))

(defgeneric save-config (obj &optional reload-config-path))
(defmethod* (save-config -> :void) ((bot lair-bot) &optional ((reload-config-path boolean) nil))
  "Saves the config object from <bot> to the config file
<bot>: The bot object to get the config object from.
<reload-config-path>: Optional, defaults to nil.
                      Passed to config-file-path to force pathname object to be recreated if a trueable."
  (with-open-file (out (find-config bot reload-config-path)
                       :external-format :utf-8 :direction :output :if-exists :supersede)
    (pprint (slot-value bot 'config) out))
  (values))

(defgeneric reply (bot msg reply-text)
  (:documentation "Instructs lair-bot to reply to a given message with a given response."))
(defmethod* (reply -> :void) ((bot lair-bot) (msg lispcord.classes:message) (reply-text string))
  "Replies to a lispcord message with the given reply-text, as the given bot."
  (lispcord:reply msg reply-text (delegate bot)))

(defgeneric parse-command (obj msg &optional formatted-content))
(defmethod* (parse-command -> boolean) ((bot lair-bot) (msg lispcord.classes:message) &optional ((formatted-content (or null string)) nil))
  "Central brain of the command processing for lair-bot. Determines based on command names what code needs to run.
Returns t if a command was found, nil if not.
<bot>: The bot object the command is being run by.
<msg>: The lispcord message object of the message being parsed.
<formatted-content>: The textual content to parse, if some pre-processing has already been done.
                     Used instead of the content slot of <msg>.
                     This can be used for recursive calls of parse-command if multi-layer processing is needed."
  (let* ((content (or formatted-content (lc:content msg)))
         (command-parts (str:words content))
         (command-name (str:upcase (car command-parts)))
         (command-list (closer-mop:class-direct-subclasses (find-class 'lair-bot.command:command)))
         (command-names (mapcar (lambda (elem)
                                  (let ((name (symbol-name (class-name elem))))
                                    (car (last (str:split ":" name))))) command-list))
         (command-position (position command-name command-names :test #'string=)))
    (if command-position
        (progn
          (lair-bot.command:run (make-instance (nth command-position command-list)) bot msg content)
          t)
        (progn
          (reply bot msg (format nil "I do not understand '~a' command! My current commands are: ~{~(~a~)~^, ~}" (car command-parts) command-names))
          nil))))

(defgeneric strip-prefix (obj msg))
(defmethod* (strip-prefix -> string) ((bot lair-bot) (msg string))
  "Simple method that strips the bot prefix from a given string.
<bot>: The bot object whose prefix should be removed.
<msg>: The string that the prefix should be removed from. Does not mutate string."
  (values (if (str:starts-with? (bot-config-prefix (slot-value bot 'config)) msg)
              (str:substring (length (bot-config-prefix (slot-value bot 'config))) t msg)
              msg)))

;;; XXX: I don't like this. Research lispcord and possibly modify it. I don't want to have to do this extra redirection.
(defgeneric create-message-dispatcher (obj))
(defmethod* (create-message-dispatcher -> function) ((bot lair-bot))
  "Creates a new event-handler function to dispatch commands that already has access to the bot object.
<bot>: lair-bot instance that will be used for command dispatches."
  (lambda* ((msg lispcord.classes:message))
    (let* ((content (lc:content msg))
           (author-id (lc:id (lc:author msg)))
           (channel (lc:channel msg))
           (channel-class (symbol-name (class-name (class-of channel))))
           (bot-user-id (lc:id (lispcord.core:bot-user (delegate bot)))))
      ;; This looks overly complex but essentially:
      ;; Process messages that are not ours, and are a DM or have the bot prefix
      (when (and (not (eql author-id bot-user-id))
                 (or (equal channel-class "DM-CHANNEL")
                     (str:starts-with? (bot-config-prefix (slot-value bot 'config)) content)))
        (parse-command bot msg (strip-prefix bot content))))))

(defgeneric connect (bot))
(defmethod* (connect -> lair-bot) ((bot lair-bot))
  "Connects lair-bot and starts handling events."
  (lispcord:connect (delegate bot))
  (lispcord:add-event-handler :on-message-create (create-message-dispatcher bot) (delegate bot))
  bot)

(defgeneric disconnect (bot))
(defmethod* (disconnect -> :void) ((bot lair-bot))
  "Disconnects lair-bot, events will no longer be handled."
  (lispcord:disconnect (delegate bot))
  (values))

(defvar* (*lair-bot* (or null lair-bot)) nil "Used for REPL, ideally should not be used in prod.")

(defun* (start-lair-bot -> lair-bot) ()
  "Sets up a new instance of lair-bot and connects to discord with a message dispatcher
The new instance is set to *lair-bot* for ease of interactive development"
  (unless *lair-bot*
    (setf *lair-bot* (make-instance 'lair-bot))
    (connect *lair-bot*))
  *lair-bot*)

(defun* (stop-lair-bot -> null) ()
  "Disconnects the current instance of lair-bot from discord and sets the global to nil,
allowing the bot object to be garbage collected."
  (disconnect *lair-bot*)
  (setf *lair-bot* nil))
