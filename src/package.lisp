;;;; package.lisp

(defpackage #:lair-bot
  (:use #:cl
        #:defstar
        #:iterate)
  (:export #:lair-bot
           #:reply
           #:state
           #:bot-state-spawn-timestamp))
