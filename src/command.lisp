;;;; command.lisp
;;; Contains implementation of the command class and related code.

(defpackage #:lair-bot.command
  (:use #:cl
        #:defstar)
  (:export #:command
           #:define-command
           #:run
           #:strip-command-name))

(in-package #:lair-bot.command)

(defclass command ()
  ()
  (:documentation "Parent class for bot commands, commands will generally inherit from this class."))

(defmacro define-command (name slots documentation description help)
  `(progn
     (defclass ,name (command) ,slots (:documentation ,documentation))
     (setf (get ',name :help) ,help)
     (setf (get ',name :description) ,description)))

(defgeneric run (command bot message input)
  (:documentation "Run a given command."))

(defgeneric strip-command-name (command input))
(defmethod* (strip-command-name -> string) ((command command) (input string))
  "Returns the command input modified by removing the command name from the beginning of it and trimming whitespace."
  (let ((trimmed-input (str:trim input))
        (command-name (symbol-name (class-name (class-of command)))))
    (if (str:starts-with? command-name trimmed-input :ignore-case t)
        (values (str:trim (str:substring (length command-name) t trimmed-input)))
        input)))
