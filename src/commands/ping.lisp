;;;; ping.lisp
;;;; Implementation of the ping command

(in-package #:lair-bot.commands)

(define-command ping ()
  "Command to return a simple reply."
  "Replies with \"pong!\"."
  "ping
Any arguments are ignored, simply respond with \"pong!\"")

(defmethod* (run -> :void) ((command ping) (bot lair-bot) (msg lc:message) (input string))
  (declare (ignore command input))
  (reply bot msg "pong!")
  (values))
