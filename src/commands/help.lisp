;;;; help.lisp
;;;; Implementation of the help command

(in-package #:lair-bot.commands)

(define-command help ()
  "Command to list data on all commands."
  "Lists the help information for lair-bot commands."
  "help
help <command-name>
When called with no arguments, lists all available commands with their description.
If called with an argument, lists the detailed help text of the command named by the argument.")

(defmethod* (run -> :void) ((command help) (bot lair-bot) (msg lc:message) (input string))
  (let ((formatted-input (strip-command-name command input)))
    (reply bot msg (if (str:empty? formatted-input)
                       (describe-commands command)
                       (describe-command command formatted-input))))
  (values))

(defgeneric describe-commands (command))
(defmethod* (describe-commands -> string) ((command help))
  (declare (ignore command))
  (let ((commands (closer-mop:class-direct-subclasses (find-class 'command)))
        (help-string (make-string-output-stream)))
    (format help-string "Available commands.~2%")
    (iter (for command in commands)
      (let* ((command-symbol (class-name command))
             (command-name (str:downcase (symbol-name command-symbol))))
        (format help-string "~a: ~a~%" command-name (get command-symbol :description))))
    (get-output-stream-string help-string)))

(defgeneric describe-command (command input))
(defmethod* (describe-command -> string) ((command help) (input string))
  (declare (ignore command))
  (let ((found-command (find-symbol (str:upcase input) 'lair-bot.commands)))
    (if found-command
        (get found-command :help)
        (format nil "ERROR: No command found named '~a'" input))))
