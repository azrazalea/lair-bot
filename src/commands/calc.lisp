;;;; calc.lisp
;;;; Implementation of the calc command

(in-package #:lair-bot.commands)

(define-command calc
    ((allowed-symbols :type (cons symbol symbol) :initform '(+ - * / sqrt expt exp
                                                             log isqrt round ceiling floor
                                                             abs sin cos tan pi)))
  "Calculator class for lair-bot that validates and evaluates arbitrary lisp form input."
  "Calculates the result of the given mathematical lisp form and returns it."
  "calc <lisp form>

Evaluates <lisp form> and returns the result. Valid operations are: +, -, \\*, /, sqrt, expt, exp, log, isqrt, ceiling, floor, abs, sin, cos, tan. The constant pi is also available.

Example: (+ pi 2 (+ 3 4) (sqrt pi) (* 5 6))
Result: 43.91404650449531d0")

(define-condition calc-invalid-input (error)
  ((bad-input :initarg :bad-input
              :initform nil
              :reader bad-input)
   (error-type :initarg :type
               :initform nil
               :reader error-type))
  (:documentation "Custom error for when our calculator detects a bad input")
  (:report (lambda (condition stream)
             (format stream "Invalid input `~A' given to calc due to ~A." (bad-input condition) (error-type condition)))))

(defmethod* (run -> :void) ((command calc) (bot lair-bot) (msg lc:message) (input string))
  (let ((calc-result nil)
        (stripped-input (strip-command-name command input)))
    (when (str:blank? stripped-input)
      (reply bot msg "ERROR: Silly user, I can't calculate nothing!")
      (return-from run (values)))
    (handler-case (setf calc-result (calc-eval command stripped-input))
      (error (c)
        (reply bot msg (format nil "ERROR: Exception caught from calc-eval -> ~a~%" c))))
    (when calc-result ;; Doesn't reply when result is nil but I don't think I care?
      (reply bot msg (format nil "Result: ~A" calc-result))))
  (values))

(defgeneric calc-validate-form (obj input)
  (:documentation "Validates lisp forms given to a calculator to ensure approved inputs."))
(defmethod* (calc-validate-form -> boolean) ((calc calc) (input number))
  "If an input to calc is simply a number it is valid."
  t)
(defmethod* (calc-validate-form -> boolean) ((calc calc) (input null))
  "If an input to calc is simply a null it is valid."
  t)
(defmethod* (calc-validate-form -> boolean) ((calc calc) (input symbol))
  "If an input to calc is a symbol, validate that it is an allowed one."
  (unless (member input (slot-value calc 'allowed-symbols))
    (error 'calc-invalid-input :bad-input input :type :not-whitelisted-symbol)
    nil)
  t)
(defmethod* (calc-validate-form -> boolean) ((calc calc) (input cons))
  "If an input to calc is a cons, recursively validate that all cons cells
contain valid inputs."
  (calc-validate-form calc (car input))
  (values (calc-validate-form calc (cdr input))))
(defmethod* (calc-validate-form -> null) ((calc calc) input)
  "If an input to calc gets to this point, it is a bad input type."
  (error 'calc-invalid-input :bad-input input :type :bad-input-type)
  nil)

(defgeneric calc-eval (obj input))
(defmethod* (calc-eval -> (or null number)) ((calc calc) (input string))
  "Reads, validates, and if validated evaluates a input string for numeric calculations."
  (let ((form (read-from-string input)))
    (when (calc-validate-form calc form)
      (values (eval form)))))
