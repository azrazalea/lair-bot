;;;; uptime.lisp
;;;; Implementation of the uptime command

(in-package #:lair-bot.commands)

(define-command uptime ()
  "Calculates the current bot uptime and responds with it in human readable units."
  "Replies with how long the current bot has been up."
  "uptime
Any arguments are ignored. Responds with human readable units representing how long the bot has been up.")

(defconstant +days-per-year+ 365)
(defconstant +years-per-decade+ 10)
(defconstant +weeks-per-year+ 52)
(defconstant +seconds-per-week+ (* local-time:+seconds-per-day+ local-time:+days-per-week+))
(defconstant +seconds-per-year+ (* local-time:+seconds-per-day+ +days-per-year+))
(defconstant +seconds-per-decade+ (* +seconds-per-year+ +years-per-decade+))

;;; Based off https://gitlab.com/procps-ng/procps/-/blob/master/proc/whattime.c
;;; since I was curious how the uptime command did it so ended up following suit.
(defun* (seconds-to-time-units -> cons) ((seconds number))
  (let* ((decades (floor (/ seconds +seconds-per-decade+)))
         (years (floor (mod (/ seconds +seconds-per-year+) +years-per-decade+)))
         (weeks (floor (mod (/ seconds +seconds-per-week+) +weeks-per-year+)))
         (days (floor (mod (/ seconds local-time:+seconds-per-day+) local-time:+days-per-week+)))
         (hours (floor (mod (/ seconds local-time:+seconds-per-hour+) local-time:+hours-per-day+)))
         (minutes (floor (mod (/ seconds local-time:+seconds-per-minute+) local-time:+seconds-per-minute+)))
         (seconds-part (mod seconds local-time:+seconds-per-minute+)))
    (list :seconds seconds-part
          :minutes minutes
          :hours hours
          :days days
          :weeks weeks
          :years years
          :decades decades)))

;;; Please note: This acts like the linux uptime command implementation and just converts the raw seconds
;;; into time intervals (i.e. 1 year is 365 days) instead of calculating the difference within timezone. This
;;; is more exact in some ways (if daylight savings just shifted, then if calculating within timezone
;;; is the uptime potentially zero or negative?) and also simpler and works more as expected.
(defmethod* (run -> :void) ((command uptime) (bot lair-bot) (msg lc:message) (input string))
  "Replies with a human readable string of the current uptime of the bot."
  (declare (ignore input command))
  (let ((time-units (seconds-to-time-units (floor (local-time:timestamp-difference
                                                   (local-time:now)
                                                   (bot-state-spawn-timestamp (state bot))))))
        (output-string (make-string-output-stream)))
    (format output-string "I have been up for")
    (iter (for unit in '(:decades :years :weeks :days :hours :minutes :seconds))
      (let ((unit-val (getf time-units unit)))
        (when (and unit-val (not (eq unit-val 0)))
          (format output-string " ~d ~(~a~)" unit-val unit))))
    (format output-string ".")
    (reply bot msg (get-output-stream-string output-string)))
  (values))
