(defpackage #:lair-bot.commands
  (:use #:cl
        #:defstar
        #:lair-bot
        #:lair-bot.command
        #:iterate)
  (:export #:calc
           #:help
           #:ping
           #:uptime))
